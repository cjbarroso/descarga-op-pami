/*
 * Baja los OPE del mes en curso y los guarda en una ubicacion establecida
 */
outdir="./";
nombrearch="op-del-mes.xlsx";
verbosico=true;
milogLevel='debug' // 'debug'
//  donde empieza la cosa
const url = 'https://efectores.pami.org.ar/pami_efectores/login.php';
const loginExitosoUrl="https://efectores.pami.org.ar/pami_nc/Novedades/novedades.php"
const usuarioPami='UP3054614365801'
const passPami='1959sanatorio'
const listadoOpUrl="https://efectores.pami.org.ar/pami_nc/OP/op_panel_listado.php"
const baseDownload="https://efectores.pami.org.ar/pami_nc/OP/"
const today=new Date();
const dd = today.getDate();
var mm = today.getMonth() + 1; //January is 0!
var yyyy = today.getFullYear();

const primer_dia = "1/"+mm+"/"+yyyy;
const hoy = dd+"/"+mm+"/"+yyyy

var casper = require('casper').create({   
    verbose: verbosico, 
    logLevel: milogLevel,
    pageSettings: {
         loadImages:  false,         // The WebPage instance used by Casper will
         loadPlugins: false,         // use these settings
         userAgent: 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_7_5) AppleWebKit/537.4 (KHTML, like Gecko) Chrome/22.0.1229.94 Safari/537.4'
    }
});
casper.options.waitTimeout = 3600000
//
// print out all the messages in the headless browser context
casper.on('remote.message', function(msg) {
    this.echo('remote message caught: ' + msg);
});
// print out all the messages in the headless browser context
casper.on("page.error", function(msg, trace) {
    this.echo("Page Error: " + msg, "ERROR");
});
var traerUrlVar = "INVALIDO";

function traerUrl() {
  casper.echo("Me preguntan por urlvar $$$$%%%%%%%%%%%%%%%%%%%%%%%%%%%");
  return traerUrlVar;
}

function setUrlVar(valor) {
  casper.echo("Seteando url var a "+valor);
  traerUrlVar = valor;
}

casper.on('resource.requested', function(resource) {
  if (resource.url.indexOf("excel") > 1) {
    this.echo("################################################################################");
    setUrlVar(resource.url);
  }
});

// LOGIN
casper.start(url, function() {
   console.log("page loaded");
   casper.capture("uno.png");
   this.fillSelectors('form#formulario', { 
        "#c_usuario": usuarioPami, 
        "#password":  passPami
    } );
    this.click("#ingresar");
   casper.capture("dos.png");
}).viewport(1600,1000);

// LOGUEO EXITOSO
casper.waitForUrl(loginExitosoUrl, function() {
    casper.capture("dos.png");
    this.echo("Now, with correct username and password, you should be signed in to the site when you see this.");
  });

// ABRO PAGINA DE LISTADO OP, CONFIGURO FORM Y PIDO export
casper.thenOpen(listadoOpUrl, function() {
  this.fillSelectors("#panel_listado", {
    "input[name=fecha_desde]": primer_dia,
    "input[name=fecha_hasta]": hoy
  });
  this.evaluate(function() {
   document.querySelector("#controlador_botonera").getElementsByClassName("btn-sm")[2].click();
   });
});

// requerido
casper.wait(1000);

// anda joya
casper.waitWhileVisible("#bloquer", function() {
  this.echo("Se fue el bloquer");
}, function() {this.echo("me canse de esperar bloquer")}, 25000);


// dentro del then porque la url del thenopen es dinamica
casper.then(function() {
  casper.thenOpen(traerUrl(), function(response) {
    this.echo("FINALIZO ESPERA ANTES DE BUSCAR EXCEL");
     // dump response header
//      require('utils').dump(response);

      // echo response body
//      this.echo(this.page.content);

      // echo response body with no tags added (useful for JSON)
      ruta = this.page.plainText
      ruta = ruta.slice(0, -1);
      ruta = ruta.substring(1);
      ruta = ruta.replace("\\", "");
      // ahora tengo que guardar el archivo
      laaurl=baseDownload+ruta
      this.echo(laaurl);
      this.download(laaurl, outdir+nombrearch);
  });
});

// EJECUTO
casper.run();
